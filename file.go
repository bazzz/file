package file

import (
	"errors"
	"io"
	"os"
	"path/filepath"
	"strings"
)

// Exists reports whether a file exists at path.
func Exists(path string) bool {
	_, err := os.Stat(path)
	return !errors.Is(err, os.ErrNotExist)
}

// NameWithoutExt returns path without the file extension.
func NameWithoutExt(path string) string {
	ext := filepath.Ext(path)
	base := filepath.Base(path)
	return base[:len(base)-len(ext)]
}

// Sanitize returns s with characters that are considered unsafe in file paths removed.
var Sanitize func(s string) string = strings.NewReplacer("\\", " ", "/", " ", ":", "", "*", "", "?", "", "\"", "", "<", "", ">", "", "|", "", "\t", "", "\n", "").Replace

// Move attempts to rename the file first, and if this did not work due to the locations being on different devices it copies and deletes.
func Move(path string, destination string) error {
	if err := os.Rename(path, destination); err != nil {
		source, err := os.Open(path)
		if err != nil {
			return err
		}
		defer source.Close()
		dest, err := os.Create(destination)
		if err != nil {
			return err
		}
		defer dest.Close()
		if _, err := io.Copy(dest, source); err != nil {
			return err
		}
		return os.Remove(path)
	}
	return nil
}
